**Task 1: Apache Log File Analysis**

This is a Bash script that counts the number of HTTP 4xx and 5xx response statuses in an Apache log file. It sends an email alert report if the cumulative number of errors exceeds 100. 

**Usage**

- How to use the script with the provided Apache log file as a parameter. For example:

_sh log_status_check.sh apache_logs_


- The script is designed to be run periodically using a cronjob. We could set up a cronjob to run the script every hour to check if the threshold for the errors has been met for that period of time.

**Example:**

_0 * * * * /path/to/log_status_check.sh /var/log/apache2/access.log_


- In the provided solution, an API work with Google App is used for sending email notifications. However, for an actual  environment, it would be necessary to set up  own SMTP service to ensure reliable email delivery.




**B. Log Storage Management**

To manage the log storage and meet the requirements of keeping logs up to 7 years, while considering the limited storage space available, These are the solutions for it.


1. Configure log rotation to compress and archive logs older than 6 months. It can be acoomplished with tool like logrotate. 

2. Move the compressed and archived logs to an cloud storage service, such as aws s3. This can help free up space on the machine while still keeping the logs available for troubleshooting and audit purpose.

3. Implement a monitoring system to track the storage usage and send alerts when the storage space runs low. 
