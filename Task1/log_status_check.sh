#!/bin/bash

# Count the number of 4xx and 5xx errors
count=$(grep -c -E " 4[0-9]{2}| 5[0-9]{2}" "$1")
echo $count
# Send an email alert if the count exceeds 100
if [ $count -gt 100 ]
then
    # Set the email parameters
    sub="High Number of Error Responses Detected"
    sender="sender@mail.com"
    receiver="receiver@gmail.com"
    body="The cumulative number of error responses in the apache log file is $count."
    
    # Send the email
    curl -s --url 'smtps://smtp.gmail.com:465' --ssl-reqd \
         --mail-from $sender \
         --mail-rcpt $receiver\
         --user $sender:"google app password" \
         -H "Subject: $sub" -H "From: $sender" -H "To: $receiver" -F \
         '=(;type=multipart/mixed' -F "=$body;type=text/plain" '=)'
fi
